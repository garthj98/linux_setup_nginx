# How to automatically run and set up nginx on an ubuntu virtual machine

### Things you will need -
- An ubuntu virtual machine 
- a secure shell key
- bitbucket repository
- a code editor (like vs code)

## Steps
- save the two scripts `bash_local.sh` and `bash_virtual.sh` in a folder
- produce a folder with a `index.html` file
- Set all bash script to executable `$ chmod +x <script_namr>`
- Update variables on bash local script `bash_local.sh`
- Run the local bash script `./bash_local.sh`

### Local bash script
- Sets variables of IP address, users and filepath names
- securely copies `$ scp` the bash script to be uploaded to the virtual machine and the folder containing the .http file 
- Runs the bash script on the virtual machine

```bash
# Set IP adress
IPADRESS=34.241.9.39
# Key loacation
KEYLOC=~/.ssh/ch9_shared.pem
# Set Users name 
USER=ubuntu
# Set website folder
WEBFOLDER=~/code/2_week/linux_setup_nginx/website
# Set virtual script loaction
SCRPTLOC=~/code/2_week/linux_setup_nginx/bash_virtual.sh

# Change permisions to make sure it is executable 
chmod +x bash_virtual.sh 

# Send bash script
scp -i $KEYLOC $SCRPTLOC $USER@$IPADRESS:~/

# Send website folder
scp -i $KEYLOC -r $WEBFOLDER $USER@$IPADRESS:~/

# Run bash script
ssh -i $KEYLOC $USER@$IPADRESS ./bash_virtual.sh  
```

#### VM bash script
- updates the virtual machine
- installs and start nginx
- moves the index.html file to /var/www/html/ 
- restarts nginx

```bash
# Update VM
sudo apt update
sudo apt-get update
sudo apt upgrade -y

# Install nginx
sudo apt install nginx -y

# Start nginx
sudo systemctl start nginx

# Move index file
sudo mv ~/website/index.html /var/www/html/ 

# Restart nginx
sudo systemctl restart nginx
```