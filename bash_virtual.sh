# Update repo of aavailable software to install
sudo apt update

# Install nginx
sudo apt install nginx -y

# Start nginx
sudo systemctl start nginx

# Move index file
sudo mv ~/website/index.html /var/www/html/ 
sudo mv ~/website/ubuntu.jpg /var/www/html/ 

# Restart nginx
sudo systemctl restart nginx