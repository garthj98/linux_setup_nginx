# Set IP adress
IPADRESS=46.51.138.96
# Key loacation
KEYLOC=~/.ssh/ch9_shared.pem
# Set Users name 
USER=ubuntu
# Set website folder
WEBFOLDER=~/code/2_week/linux_setup_nginx/website
# Set virtual script loaction
SCRPTLOC=~/code/2_week/linux_setup_nginx/basvirtual.sh

# Change permisions to make sure it is executable 
chmod +x bash_virtual.sh 

# Send bash script
scp -i $KEYLOC $SCRPTLOC $USER@$IPADRESS:~/

# Send website folder
scp -i $KEYLOC -r $WEBFOLDER $USER@$IPADRESS:~/

# Run bash script
ssh -i $KEYLOC $USER@$IPADRESS ./bash_virtual.sh  

open http://$IPADRESS